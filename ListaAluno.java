
public class ListaAluno {
	
	Aluno[] alunos = new Aluno[60];
	int tamanho = 0;
	
	public boolean estaVazia() {
		return (tamanho == 0);
	}
	public boolean estaCheia() {
		return (tamanho == alunos.length);
	}
	public int tamanhoLista() {
		return tamanho;
	}

	public boolean compara(String c1, String c2) {
		return (c1.equals(c2));	
	}
	
	public int retornarPosicao(int rgm) {
		for (int i = 0; i < tamanho; i++) {
			if (alunos[i].getRgm() == rgm) {
				return i;
			}
		}
	return -1;
	}
	
	public void deslocarParaDireita(int pos) {
		for(int i = tamanho; i > pos; i--) {
			alunos[i] = alunos[i -1];
		}
	}
	
	public void deslocarParaEsquerda(int pos) {
		for(int i = pos; i < (tamanho - 1); i++) {
			alunos[i] = alunos[i+1];
		}
	}
	
	public int inserirAluno(Aluno aluno) {
		if(estaCheia()) {
			return -1;
		}
		if (tamanho == 0) {
			alunos[0] = aluno;
			tamanho++;
		} else {
			if(alunos[0].getRgm() < aluno.getRgm()) {
				for(int j = 0; j < tamanho; j++) {
					if(alunos[j].getRgm() > aluno.getRgm() && alunos[j-1].getRgm() < aluno.getRgm()) {
						deslocarParaDireita(j);
						alunos[j] = aluno;
						tamanho++;
					}
					else if(alunos[j].getRgm() < aluno.getRgm() && alunos[j+1] == null) {
						alunos[tamanho] = aluno;
						tamanho++;
					}
				}
			}else {
				deslocarParaDireita(0);
				alunos[0] = aluno;
				tamanho++;
			}
			
			return 1;			
		}
		return tamanho;
	}


	public void deletarAluno(int pos)	{
		if(retornarPosicao(pos) > -1) {
			deslocarParaEsquerda(retornarPosicao(pos));
			tamanho--;
		}else {
			System.out.println("RGM n�o encontrado.");
		}
	}

	public void ProcurarAluno(int rgm) {
		System.out.println("------------------------------------------");
		System.out.println("=	          Buscar Aluno             =");
		System.out.println("------------------------------------------");
		if ((retornarPosicao(rgm) >= tamanho) || (retornarPosicao(rgm) < 0)){
			System.out.println("Aluno n�o cadastrado\n");
		}else {
			System.out.println("RGM: " + alunos[retornarPosicao(rgm)].getRgm());
			System.out.println(alunos[retornarPosicao(rgm)].listadisciplina.mostrarDisciplina());
		}
	}
	
	public void MostrarLista() {
		System.out.println("------------------------------------------");
		System.out.println("=	          Lista dos Aluno          =");
		System.out.println("------------------------------------------");
		if(tamanho == 0) {
			System.out.println("N�o existe alunos cadastrados.\n");
			System.out.println("------------------------------");
		}
		else {
			for (int i = 0; i < tamanho; i++){
				System.out.println("RGM: " + alunos[i].getRgm());
				System.out.println(alunos[i].listadisciplina.mostrarDisciplina());
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
		
