
public class No {
	private Disciplina dis;
	private No prox;
	
	public No(Disciplina disc) {
		this.dis = disc;
		this.prox = null;
	}
	
	public Disciplina getDis() {
		return dis;
	}
	public void setDis(Disciplina dis) {
		this.dis = dis;
	}
	public No getProx() {
		return prox;
	}
	public void setProx(No prox) {
		this.prox = prox;
	}
	
}
