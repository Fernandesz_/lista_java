

public class ListaDisciplina {
	private No prim;
	private No ult;
	private int qntdNo;
	
	public ListaDisciplina() {
		this.prim = null;
		this.ult = null;
		this.qntdNo = 0;
		
	}//-Construtor
	
	public No getPrim() {
		return prim;
	}
	
	public void SetPrim(No prim) {
		this.prim = prim;
	}
	
	public No getUlt() {
		return ult;
	}
	
	public void SetUlt(No ult) {
		this.ult = ult;
	}
	
	public int getQntdNo() {
		return qntdNo;
	}
	
	public boolean eVazia() {
		return(this.prim == null);
	}//Esta Vazia?

	public void adicionarDisciplina(Disciplina dis) {
		No NovoNo = new No(dis);
		if(this.eVazia()){
			this.ult = NovoNo;
		}
		NovoNo.setProx(this.prim);
		this.prim = NovoNo;
		this.qntdNo++;
	}
	
	public boolean removerDisciplina(String cadeira) {
		No atual = this.prim;
		No ant = null;
		if(this.eVazia()) {
			return false;
		}
		else {
			while((atual != null) && (!atual.getDis().getCadeira().equals(cadeira))){
				ant = atual;
				atual = atual.getProx();
			}
			if(atual == this.prim) {
				if(this.prim == this.ult) {
					this.ult = null;
				}
				this.prim = this.prim.getProx();
			}
			else {
				if(atual == this.ult) {
					this.ult = ant;
				}
				ant.setProx(atual.getProx());
			}
			this.qntdNo--;
			return true;
		}
	}//-remover
	
	public String pesquisarNo(String cadeira){
		String msg = "";
		No atual = this.prim;

		while((atual != null) && (!atual.getDis().getCadeira().equals(cadeira))){
			atual = atual.getProx();
		}
		if(atual == null) {
			msg = "Disciplina n�o encontrada.";
		}
		else {
			   msg += "Cadeira: "+atual.getDis().getCadeira()+ "\n"+
					  "Nota: "+atual.getDis().getNota() + "\n";

					
			}
		return msg;
	}
	
	public String mostrarDisciplina() {
		String msg = "";
		if(eVazia()) {
			msg = "N�o possui disciplina cadastrada!\n";
		}else {
			No atual = this.prim;
			while(atual != null) {
				msg += "Materia: "+atual.getDis().getCadeira() + "\n"+
						"Nota: " + atual.getDis().getNota()+ "\n";
				atual = atual.getProx();
			}
		}
		return msg;
	}

}
