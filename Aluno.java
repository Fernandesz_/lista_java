
public class Aluno {
	private int rgm;
	ListaDisciplina listadisciplina;
	
	public Aluno() {
		this.rgm = getRgm();
		listadisciplina = new ListaDisciplina();
	}
	
	public int getRgm() {
		return rgm;
	}
	public void setRgm(int rgm) {
		this.rgm = rgm;
	}
	
	public void adicionarDisciplina(Disciplina disc) {
		listadisciplina.adicionarDisciplina(disc);
	}
	
	public void mostrarDisciplina() {
		listadisciplina.mostrarDisciplina();
	}
}
