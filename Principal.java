import java.util.Scanner;



public class Principal {
	public static char menu() {
		String msg;
		Scanner teclado = new Scanner(System.in);
		System.out.println("------------------------------------------");
		System.out.println("=	     Escolha uma op��o            =");
		System.out.println("------------------------------------------");
		System.out.println("=	[1]. Cadastrar aluno.             =");
		System.out.println("=	[2]. Imprimir lista.              =");
		System.out.println("=	[3]. Localizar aluno.             =");
		System.out.println("=	[4]. Excluir Aluno.               =");
		System.out.println("=	[5]. Sair do programa.            =");
		System.out.println("------------------------------------------");
		System.out.print("-> ");
		msg = teclado.next();
		return msg.charAt(0);
	}

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		ListaAluno lista = new ListaAluno();
		Aluno aln;
		char opcao;
		int num;
		String opcao2;
		do {
			opcao = menu();
			switch(opcao) {
			case '1':
				System.out.println("--------- Cadastrando Aluno ---------");
				aln = new Aluno();
				System.out.print("\nDigite seu RGM: ");
				aln.setRgm(teclado.nextInt());
				lista.inserirAluno(aln);
				System.out.println("\nDeseja adicionar disciplinas?");
				System.out.print("-> ");
				opcao2 = teclado.next();
				while((opcao2.equals("Sim")) || (opcao2.equals("sim"))) {
					Disciplina disc = new Disciplina();
					String cadeira;
					double nota;
					teclado.nextLine();
					System.out.println("\nDigite a cadeira: ");
					System.out.print("-> ");
					cadeira = teclado.nextLine();
					disc.setCadeira(cadeira);
					System.out.println("\nDigite a nota da cadeira: ");
					System.out.print("-> ");
					nota = teclado.nextDouble();
					disc.setNota(nota);
					aln.adicionarDisciplina(disc);
					System.out.println("\nPara adicionar mais disciplinas digite \"Sim\" ");
					System.out.print("-> ");
					teclado.nextLine();
					opcao2 = teclado.nextLine();
				}
				System.out.println("----------------------------------------------------------");
				System.out.println("                 Cadastrado com sucesso!");
				System.out.println("----------------------------------------------------------");
				break;
				case '2':
					lista.MostrarLista();
					break;
				case'3':
					System.out.println("Digite o RGM que deseja procurar:");
					System.out.print("-> ");
					num = teclado.nextInt();
					lista.ProcurarAluno(num);
					break;
				case'4':
					System.out.println("Digite o RGM que deseja excluir:");
					System.out.print("-> ");
					num = teclado.nextInt();
					lista.deletarAluno(num);
					System.out.println("----------------------------------------------------------");
					System.out.println("                Deletado com sucesso!");
					System.out.println("----------------------------------------------------------");
					lista.MostrarLista();
					break;
				case'5':
					System.out.println("Fim do programa.");
					break;
				default:
					System.out.println("Op��o inv�lida, tente novamente!");
		}	
		}while(opcao != '5');
		teclado.close();
		System.exit(5);

	
	
	}




}
